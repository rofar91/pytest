import time
import pytest


@pytest.fixture(scope='class', autouse=True)
def suite_data():
    print("\n> Suite setup")
    yield
    print("> Suite teardown")


@pytest.fixture(scope='function')
def case_data():
    print("   > Case setup")
    yield time.time()
    print("\n   > Case teardown")


@pytest.fixture(scope="function", params=[
    ("123456", "123456?"),
    ("123", "123!"),
    ("12345", "12345."),
])
def param_test(request):
    return request.param
