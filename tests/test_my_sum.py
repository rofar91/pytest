from one import summa


def test_summa():
    """Check that a+b=c"""
    assert summa(1, 2) == 3, "Should be 3"
