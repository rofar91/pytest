from strange_string import strange_string_func


def test_strange_string(param_test):
    (test_input, expected_output) = param_test
    result = strange_string_func(test_input)
    print("input: {0}, output: {1}, expected: {2}".format(test_input, result, expected_output))
    assert result == expected_output
